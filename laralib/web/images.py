"""
________________________________________________________________________

:PROJECT: LARA

*web_tools*

:details: modules for
         -
:file:    web_tools.py
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          2019-12-11
:date: (last modification) 2019-12-17

.. note:: -
.. todo:: - #

________________________________________________________________________
"""
__version__ = "0.0.1"

import os
import sys
import logging

import requests


def downloadImage(image_url, image_path, output_image_filename):
    """ download an image from a given url """

    if not os.path.exists(image_path):
        os.makedirs(image_path)

    output_image_filename_full = os.path.join(image_path, output_image_filename )

    try:
        structure_img = requests.get(image_url).content
        with open(output_image_filename_full, 'wb') as img_file:
            img_file.write(img_data)
    except Exception as err:
        sys.stderr.write(f"ERROR ({err}): Cannot retrieve image from {image_url} !\n")
