"""
________________________________________________________________________

:PROJECT: LARA

*chemspider.py - a chemistry information retrieval module*

:details: This module retrieves chemical data from various databases
         - 
:file:    chemspider.py
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20190307
:date: (last modification) 20190307

.. note:: -
.. todo:: - #    

________________________________________________________________________
"""
__version__ = "0.0.1"

import logging

from chemspipy import ChemSpider
from os.path import expanduser
import configparser  # !!! renamed in python3.0 !!!

class SubstanceReaderChemSpider():
    """ Class doc """
    def __init__ (self):
        """ Class initialiser """
        pass
            
#~ config_file_path = expanduser("~") + '/.config/ChemSpider/chemspider.conf'
#~ try: 
    #~ config = configparser.RawConfigParser()
    #~ config.read(config_file_path)
    #~ self.cs_token = config.get('Auth', 'token')        


    def chemSpiderCompoundByID(self, subst_id):
        ''' s. http://blog.matt-swain.com/post/16893587098/chemspipy-a-python-wrapper-for-the-chemspider '''
        from chemspipy import ChemSpider

        #~ logging.debug(self.cs_token)
        if self.cs_token : 
            cs = ChemSpider(self.cs_token)
            compound = cs.get_compound(subst_id)
            
            if compound:
                substance = Substance()
            
                logging.debug(compound.common_name)
                substance["name"] = compound.common_name
                substance["sumformula"] = compound.molecular_formula
                substance["SMILES"] = compound.smiles
                substance["InChI"] = compound.inchi
                substance["InChIKey"] = compound.inchikey #InChIKey.
                substance["StdInChI"] = compound.stdinchi
                substance["StdInChIKey"] = compound.stdinchikey # Standard InChIKey.
                #~ substance["PubChemFingerprint"] = compound.fingerprint
                #self.substance["InChI"] = compound.average_mass
                substance["mwt"] = compound.molecular_weight # Molecular weight.
                #self.substance["InChI"] = compound.monoisotopic_mass: Monoisotopic mass.
                #alogp: AlogP.
                substance["XlogP"] = compound.xlogp #: XlogP.
                #~ substance["synonyms"] = compound.synonyms
                #~ substance["mol_raw"] = compound.mol_raw # Unprocessed MOL file.
                #~ substance["mol_2D"] = compound.mol_2d #: MOL file containing 2D coordinates.
                substance["mol_3D"] = compound.mol_3d
                #~ substance["png"] = compound.image # 2D depiction as binary data in PNG format.
                #~ substance["spectra"] = compound.spectra #: List of spectra.
                
                self.substances.append(substance)
