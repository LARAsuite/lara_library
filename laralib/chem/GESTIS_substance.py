"""
________________________________________________________________________

:PROJECT: LARA

*GESTIS_substance.py - a chemistry information retrieval module*

:details: This module retrieves chemical data from GESTIS chemical safety database
         - 
:file:    chemspider.py
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20190307
:date: (last modification) 20190307

.. note:: -
.. todo:: - #    

________________________________________________________________________
"""
__version__ = "0.0.1"

import logging

