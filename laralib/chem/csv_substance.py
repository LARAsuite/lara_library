"""
________________________________________________________________________

:PROJECT: LARA

*chemspider.py - a chemistry information retrieval module*

:details: This module retrieves chemical data from various databases
         - 
:file:    chemspider.py
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20190307
:date: (last modification) 20190307

.. note:: -
.. todo:: - #    

________________________________________________________________________
"""
__version__ = "0.0.1"

import logging

class SubstanceCSVReader():
    """ Class doc """
    def __init__ (self):
        """ Class initialiser """
     
