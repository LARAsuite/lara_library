"""_____________________________________________________________________

:PROJECT: lara_library

*lara_library projects*

:details: lara_library projects

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20190113
:date: (last modification) 20190127

.. note:: -
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.0.4"

import logging

#~ from lara_metainfo.models import ItemClass, MetaInfo, Tags
#~ from lara_people.models import LaraUser
#~ from lara_projects.models import ProjectItem
#~ from lara_containers.models import ContainerLayout, Container
#~ from lara_devices.models  import Device
#~ from lara_data.models import Evaluation
#~ from lara_projects.models import ProjectItem

#~ settings.configure()

class ProjectAbstr:
    def __init__(self, parent=None,
                 parent_project="root",
                 name="DefaultProjName",
                 title="DefaultProjectTitle",
                 description="",
                 user= ["LAR"],
                 startDatetime="1969-06-09 12:21",
                 parameters="param",
                 containers=[],
                 wells=[],
                 remarks="",
                 eval=None):
        """ :param parent: parent evalution
            :param parameters: evaluation parameters, a JSON string
        """
        
        self.parent = parent
        self.name = name
        self.title = title
        self.description = description
        self.parameters = parameters
        self.user = user
        self.startDatetime = startDatetime
        self.remarks = remarks
        self.evaluation = eval
        
        logging.debug("- init name:{}".format(self.name) )
        
        #~ try:
            #~ self.proj_root = ProjectItem.objects.get(name="root")
        #~ except ObjectDoesNotExist:   # no root item exists, makeing one
            #~ self.proj_root = ProjectItem.objects.create( name="root", 
                       #~ title="root item of project item tree", 
                       #~ description="root item of project item tree", 
                       #~ start_datetime=datetime.datetime.now(datetime.timezone.utc),
                       #~ remarks="root needs to be present")
                 
        # if self.implementation is set to None, it will use
        # the fallback simulation mode as default
        # if required, one could also create a simulation module and set this to the default implementation, like:
        #~ self.injectImplementation(GreetingProviderSimulation())

        self.implementation = None # this corresponds to the simple, fallback simulation mode
        
        self.addDB()
    
    def addDB(self):
        """ :param [param_name]: [description]"""
        logging.debug("dummy add db - project: {}".format(self.name) )
         
        
    def injectImplementation(self, implementation):
        """call: object.injectImplementation(my_implement()) """
        self.implementation = implementation
            
            
     #~ if self.implementation is not None:
            #~ return self.implementation.SayHello(request, context)
        #~ else:
        
    def printNamesRec(self):
        """recursively print parameters
           :param - : -"""
        
        try:
            self.parent.printNamesRec()
            
        except Exception as err:
            logging.error("Top of tree reached, NO-PARENT Error ns 1 {}".format(err) )
            
        logging.debug("proj. para name: {}".format(self.name) )
        
            
    def addDBRec(self):
        """recursively adding Project to database
          :param none: -"""
        
        self.parent_it=self.proj_root
        
        try:
            self.parent.addDBRec()
            
        except Exception as err:
            logging.error("Top of tree reached, NO-PARENT Error ns 1 {}".format(err) )
            
        logging.debug("adding proj. to DB: {}".format(self.name) )
        
        #~ proj_it = ProjectItem.objects.create( name=self.name, 
                                            #~ title=self.title, description=self.description,
                                            #~ start_datetime=self.startDatetime,
                                            #~ parameters = self.parameters, 
                                            #~ remarks=self.remarks,
                                            #sample_source=it_sample_source,
                                            #method=it_method, device=it_device, item_class=it_class, 
                                            #~ parent=self.parent_it) #, reference_experiment=it_reference_experiment)
                                            
        #~ if item_class == 'experiment':
            #~ parent_it.experiments.add(proj_it) # connection might be not necessary
            #~ self.exp_dict[new_exp_name] = proj_it
            
        #~ proj_it.save()
        
        #~ self.parent_it = proj_it
    
    def evalBottomTop(self):
        """evaluate from bottom (last evaluation) til top (first evaluation)
          :param -: -"""
        
        logging.debug("eval bottom->top: para ns 1: {}".format(self.name) )
        
        try:
            self.parent.evalBottomTop()
        except Exception as err:
            logging.error("Top reached: {}".format(err) )
            
    def evalTopBottom(self, next_item=None ):
        """evaluate from top (first evaluation) til bottom (last evaluation).
           It first moveves to the first entry in the evaluation chain and then 
           recursively down to the calling object.
           :param next_item=None : next item """
        
        try:
            self.parent.evalTopBottom(next_item=self)
        except Exception as err:
            logging.error("Top reached ns 1 {}".format(err) )
            
        logging.debug("eval top->bottom: self.name ns 1: {}".format(self.name) )
        

class ExperimentAbstr(ProjectAbstr):
    """ Class doc """
    
    def __init__ (self, parent=None,
                 name="DefaultName",
                 title="DefaultProjectTitle",
                 description="",
                 user= ["LAR"],
                 startDatetime="1969-06-09 12:21",
                 parameters="param",
                 wells=[],
                 remarks="",
                 containers=[""],
                 eval=None):
        super().__init__( parent=parent,
                 name=name,
                 title=title,
                 description=description,
                 user=user,
                 startDatetime=startDatetime,
                 parameters=parameters,
                 containers=containers,
                 wells=wells,
                 remarks=remarks,
                 eval=eval)
        """ Class initialiser """
        
        self.containers = containers
        
        logging.debug("containers: {}".format(containers) )
        
        self.addDB()
        
        
    def printNamesRec(self):
        """recursively print parameters
           :param - : -"""
        
        logging.debug("experiment. para name: {}".format(self.name) )
        
        try:
            self.parent.printNamesRec()
            
        except Exception as err:
            logging.error("Top of tree reached, NO-PARENT Error ns 1 {}".format(err) )
            
    
        
        
