"""
________________________________________________________________________

:PROJECT: LARA

*parameter_optimisation.py - starting parameter optimisation for machine learning *

:details: 
         - 
:file:    parameter_optimisation.py
:authors:  Stefan Born (stefan.born@tu-berlin.de)
           mark doerr  (mark@uni-greifswald.de)

:date: (creation)          20190626
:date: (last modification) 20190626

.. note:: -
.. todo:: - #    

________________________________________________________________________
"""
__version__ = "0.0.1"

import logging
import numpy as np
from scipy.special import erf

from sklearn.linear_model import Lasso, Ridge
from sklearn.model_selection import KFold,LeaveOneOut,RepeatedKFold
from sklearn.metrics import mean_squared_error, mean_absolute_error


def calcCoefficientsLasso(X, y, alpha=0.0, num_iter=500, labels=[] ):
    """ :param [param_name]: [description]"""
            
    coefs = []
    
    for i in range(num_iter):
        subset = np.random.choice(range(X.shape[0]), 9*X.shape[0]//10,replace=False)
        model = Lasso(alpha=alpha)
        model.fit(X[subset,:],np.log(y[subset]+1e-2))
        coefs.append(model.coef_)
        
    mean_coefs = np.mean(coefs,axis=0)
    
    std_coefs = np.std(coefs,axis=0)/np.sqrt(len(coefs))
 
    # for debugging
    printCoeffs(labels, mean_coefs,std_coefs )
    
    return mean_coefs, std_coefs
    

def printCoeffs(labels, mean_coefs,std_coefs ):
    """ :param [param_name]: [description]"""
            
    for lab, c,s in zip(labels, mean_coefs,std_coefs):
        print('{:11s} {:>+1.5f} sigma= {:>+1.5f}'.format(lab,c,s))


# selction of the best coeff. based on the Benjamini Hochberg procedur/criterion 1995 !!
# at most alpha=0.2 (=20%) of the kept coefficients are irrelevant or negative (in the linear model)
# = good selection of good active variants

def Phi(x):
    """ distribution function of the normal distribtion prob.of -8 and x"""
    return 0.5*(1+erf(x/np.sqrt(2)))
        
        
def coefSelectBenHochb(mean_coefs, std_coefs, labels=[], alpha = 0.2 ):
    """ :param alpha:  level of significance, = false discovery rate (s. Benj. Hochb. criterion) 
    """
    # at most 20% of the kept coefficients are irrelevant or negative (in the linear model) 
    
    zs = []
    for i,(c,s) in enumerate(zip(mean_coefs,std_coefs)):
        if s>0:
            zs.append((i,Phi(-c/s))) # prob. 
        elif c==0:
            zs.append((i,1))
        elif c>0:
            zs.append((i,0))
        else:
            zs.append((i,1))
            
    zs.sort(key = lambda t:t[1])
    m = len(zs)
   
    print("------ at most 20% of the kept coefficients are irrelevant or negative (in the linear model) " )
    
    for i,(j,p) in enumerate(zs):
        if p>((i+1)/m)*alpha:  # = Benj. Hochb. procedur/criterion 1995 !!
            break
        print('p:  {:1.5e} {:11s} index {:3d} mean = {:>+1.5f} sigma= {:>+1.5f}'.format(p, labels[j],j,mean_coefs[j],std_coefs[j]))
        
    # at most 10% of the left out coefficients are relevant or neutral (in the linear model)


    zs = []
    for i,(c,s) in enumerate(zip(mean_coefs,std_coefs)):
        if s>0:
            zs.append((i,Phi(c/s)))
        elif c==0:
            zs.append((i,1))
        elif c>0:
            zs.append((i,1))
        else:
            zs.append((i,1))
        
    zs.sort(key = lambda t:t[1])
    m = len(zs)
    alpha = 0.1
    
    return
    
    print ("Leave out:")
    
    print("----------- at most 10% of the left out coefficients are relevant or neutral (in the linear model) " )
    # = variants that should be thrown away 
    
    
    for i,(j,p) in enumerate(zs):
        if p>((i+1)/m)*alpha:
            break
        print('p:  {:1.5e} {:11s} index {:3d} mean = {:>+1.5f} sigma= {:>+1.5f}'.format(p, labels[j],j,mean_coefs[j],std_coefs[j]))

