"""_____________________________________________________________________

:PROJECT: lara_library

*lara_library evaluation*

:details: lara_library evaluation

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20190113
:date: (last modification) 20190113

.. note:: -
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.0.3"

import logging

class Evaluate:
    def __init__(self, parent=None, parameters=""):
        """ :param parent: parent evalution
            :param parameters: evaluation parameters, a JSON string
        """
        
        self.parent = parent
        self.parameters = parameters
        
        logging.debug("- para ns 1:{}".format(parameters) )
        
        
    def recPrintParameters(self):
        """recursively print parameters
           :param - : -"""
        
        para=self.parameters
        
        logging.debug("para ns 1: {}".format(para) )
        
        try:
            self.parent.recPrintParameters()
            
        except Exception as err:
            logging.error("No-parent Error ns 1 {}".format(err) )
    
            
    def evalBottomTop(self):
        """evaluate from bottom (last evaluation) til top (first evaluation)
          :param -: -"""
        
        para=self.parameters
        
        logging.debug("eval bottom->top: para ns 1: {}".format(para) )
        
        try:
            self.parent.evalBottomTop()
        except Exception as err:
            logging.error("Top reached: {}".format(err) )
            
    def evalTopBottom(self, next_item=None ):
        """evaluate from top (first evaluation) til bottom (last evaluation).
           It first moveves to the first entry in the evaluation chain and then 
           recursively down to the calling object.
           :param next_item=None : next item """
        
        para=self.parameters
        
        try:
            self.parent.evalTopBottom(next_item=self)
        except Exception as err:
            logging.error("Top reached ns 1 {}".format(err) )
            
        logging.debug("eval top->bottom: para ns 1: {}".format(para) )
        
