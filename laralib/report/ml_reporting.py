"""
________________________________________________________________________

:PROJECT: LARA

*parameter_optimisation.py - starting parameter optimisation for machine learning *

:details: 
         - 
:file:    parameter_optimisation.py
:authors:  Stefan Born (stefan.born@tu-berlin.de)
           mark doerr  (mark@uni-greifswald.de)

:date: (creation)          20190626
:date: (last modification) 20190626

.. note:: -
.. todo:: - #    

________________________________________________________________________
"""
__version__ = "0.0.1"

import logging
import numpy as np
from sklearn.linear_model import Lasso, Ridge

from sklearn.model_selection import KFold,LeaveOneOut,RepeatedKFold
from sklearn.metrics import mean_squared_error, mean_absolute_error

    
