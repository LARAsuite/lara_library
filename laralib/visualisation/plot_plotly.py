"""_____________________________________________________________________

:PROJECT: LARAsuite

*plotly plots.*

:details: plotly plots.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20200628

.. note:: -
.. todo:: - 

________________________________________________________________________
"""

__version__ = "0.0.1"


from plot import Plot

class PlotPlotly(Plot):
    def __init__(self):
        pass
