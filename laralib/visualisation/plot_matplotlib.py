"""_____________________________________________________________________

:PROJECT: LARAsuite

*matplotlib plots.*

:details: matplotlib plots.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20200628

.. note:: -
.. todo:: - replace data_fl by additional columns in data and overlay list with column names

________________________________________________________________________
"""

__version__ = "0.0.1"


import logging
from laralib.visualisation.plot import Plot

import matplotlib.pyplot as plt
import string
import numpy as np

class FigCaption():
    def __init__(self,text: str = ""):
        self.text = text
    def _repr_html_(self):
        return f'<center>{self.text}</center>'
    def _repr_latex_(self):
        return f'\\begin{{center}}\n{self.text}\n\\end{{center}}'

class PlotMatplotlib(Plot):
    def __init__(self, data=None, plotColumn: str = None,  groupingCol: str = "", barcode: str = "",
                 title: str = "", caption = "",
                 overview: bool = False, preview: bool = False, overlay: bool = False, autoscale_overlay: bool = False,
                 equalScale: bool = True, color='red',
                 plotWidth: int = 2400, plotHeight: int = 1600, dpi: int = 240, plotQuality: float = 1.0,
                 filename="LA_Plot", outputFormat='png'):
        super().__init__(data=data, plotColumn=plotColumn,  groupingCol=groupingCol, barcode=barcode,
                         title=title, caption=caption,
                         overview=overview, preview=preview, overlay=overlay, autoscale_overlay=autoscale_overlay,
                         equalScale=equalScale,
                         plotWidth=plotWidth, plotHeight=plotHeight, dpi=dpi, plotQuality=plotQuality,
                         filename=filename, outputFormat=outputFormat)
        self.plot_overlay_data = []
        self.color = color

    def _overview_plot(self):
        self.fig, self.axs = plt.subplots(
            8, 12, figsize=(self.plot_width, self.plot_height))
        print("overvw4")

        if self.equal_scale:
            y_min = min(self.data[self.plot_column])
            y_max = max(self.data[self.plot_column])

        for curr_well in self.well_names():
            curr_well_df = self.data.loc[self.data["well_name"] == curr_well]
            curr_row = int(curr_well_df.iloc[0].loc["row_num"])
            curr_col = int(curr_well_df.iloc[0].loc["col_num"])

            curr_max = max(curr_well_df[self.plot_column])

            max_line_y = [curr_max] * len(curr_well_df["delta_time_unit"])

            if self.equal_scale:
                self.axs[curr_row, curr_col].set_ylim([y_min, y_max])
            self.axs[curr_row, curr_col].plot(
                curr_well_df["delta_time_unit"], curr_well_df[self.plot_column], color=self.color)
            self.axs[curr_row, curr_col].plot(
                curr_well_df["delta_time_unit"], max_line_y, color="grey")
            self.axs[curr_row, curr_col].text(1, curr_max*1.01, curr_max, color=self.color, fontsize=8)


            for overlay_data, offset, color_overlay in self.plot_overlay_data:
                curr_well_overlay_df = overlay_data.loc[overlay_data["well_name"] == curr_well]

                if self.autoscale_overlay:
                    curr_well_overlay_df["value_norm"] = curr_well_overlay_df["value"] / curr_well_overlay_df.value.max()
                    curr_well_overlay_df["value_scaled"] =  curr_well_overlay_df["value_norm"] * self.data.max()
                    self.axs[curr_row, curr_col].plot(
                       curr_well_overlay_df["delta_time_unit"], curr_well_overlay_df["value_scaled"]+offset, color=color_overlay)
                else:
                    self.axs[curr_row, curr_col].plot(
                       curr_well_overlay_df["delta_time_unit"], curr_well_overlay_df["value"]+offset, color=color_overlay)

            #self.axs[curr_row, curr_col].axis([0, 10, 0, 10])
            self.axs[curr_row, curr_col].text(1, y_max*1.01 , curr_well, color='green', fontsize=11)
            

        self.fig.tight_layout()  # show()
        #plt.figtext(0.5, 0.01, self.caption, wrap=True, horizontalalignment='center', fontsize=14)
        #plt.figtext(0.5, 0.01, self.caption, wrap=True, horizontalalignment='center', fontsize=14)

    def add_plot_data(self, plot_overlay_data, offset, color):
        self.plot_overlay_data.append((plot_overlay_data, offset, color))

    def heatmap(self, sig_digits: int = 2, vmin: float = None, vmax: float = None, norm=None):
        # add auto generation function from geometry
        # to reshape data frame with 8 rows/12cols use e.g.: df['col_to_plot'].to_numpy().reshape(8,12)

        num_rows = self.data.shape[0]
        num_cols = self.data.shape[1]

        row_names = list(string.ascii_uppercase[:num_rows])
        col_names = list(range(1, num_cols + 1))

        self.fig, ax = plt.subplots(figsize=(10, 10))

        im = ax.imshow(self.data, vmin=vmin, vmax=vmax, norm=norm)
        ax.set_xticks(np.arange(len(col_names)))
        ax.set_yticks(np.arange(len(row_names)))
        # ... and label them with the respective list entries
        ax.set_xticklabels(col_names)
        ax.set_yticklabels(row_names)

        #plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

        # Loop over data dimensions and create centered text annotations.
        for i in range(num_rows):
            for j in range(num_cols):
                text = ax.text(j, i, self.data[i, j].round(
                    sig_digits), ha="center", va="center", color="w")

        self.fig.tight_layout()

    def boxplot(self, labels: [str], colors: list = None):
        """
        s. https://matplotlib.org/3.3.2/gallery/statistics/boxplot_color.html#sphx-glr-gallery-statistics-boxplot-color-py
        """

        self.fig, ax = plt.subplots(figsize=(10, 10))

        bplot = ax.boxplot(self.data,
                           vert=True,  # vertical box alignment
                           patch_artist=True,  # fill with color
                           labels=labels)  # will be used to label x-ticks
        if colors is not None:
            for patch, color in zip(bplot['boxes'], colors):
                patch.set_facecolor(color)

        bplot['boxes']

        # ax.set_title(self.title)

    def show(self):
        print("show fig")
        
        if self.overview_plot:
            self._overview_plot()

        plt.show()
        if self.caption != "" :
            self.capt = FigCaption(self.caption)
            

    def save(self, filename: str = None, output_format: str = None):
        if filename is None:
            filename = self.filename
        if output_format is None:
            output_format = self.output_format
        self.fig.savefig(f"{filename}.{output_format}",
                         output_format=output_format, dpi=self.dpi)
