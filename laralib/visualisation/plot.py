"""_____________________________________________________________________

:PROJECT: LARAsuite

*generic multipurpose plots.*

:details: generic multipurpose plots.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20200628

.. note:: -
.. todo:: - 

________________________________________________________________________
"""

__version__ = "0.0.1"

import pandas as pd

# abstract baseclass

class Plot:
    def __init__(self, data = None, plotColumn: str ="",  groupingCol: str = "", barcode: str ="", 
                       title: str ="", caption: str = "",
                       overview: bool = False, preview: bool = False, overlay: bool=False,  autoscale_overlay: bool=False, 
                       equalScale: bool = True,
                       plotWidth: int = 1200, plotHeight: int = 1024, dpi: int = 240, plotQuality: float = 1.0, filename = "LA_Plot", 
                       outputFormat='png'):

        # errorBars: bool = False, plotPeaks:bool = False,
        # wellNames: bool = False, description: bool = False, markPlotSegments: bool = False,
        # markAverage: bool = True, markBest: bool = False, plotRef: bool=True, plotBestModel: bool=False,
        # plotType: str = "b", plotChar: str = "", charScale: float = 1.0, lineType: str = "-", lineWidth: float = 1.0, 
        # lineCol: str ='red', fillCol: str = "lightgrey", 
        # numCol: int = 20, customCol: bool = False,
        # panelGeometry: list = [8,12],  plotTitle: str = "",  plotLegend: bool=False,
        # ylim: list = None,   xlim: list = None, plotAxes: bool = False, 
        # xlab: str = "", ylab: str = "", zlab: str = "",
        # plotGrid: bool=False,
        # plotBg: str = "white", transp: float = 1.0,
        # barWidth: float = 0.5, barDistance: float = 0.8, theta: float = -60.0, phi: float = 15.0 , scale: float = 1.0,

        self.data = data
        self.plot_column = plotColumn
        self.barcode = barcode
        self.title = title
        self.caption = caption
        self.overview_plot = overview
        self.overlay_plot = overlay
        self.autoscale_overlay = autoscale_overlay
        self.equal_scale = equalScale
        self.plot_width = plotWidth
        self.plot_height = plotHeight
        self.dpi = dpi 
        self.filename = filename
        self.output_format = outputFormat


    def well_names(self):
        codes, uniques = pd.factorize(self.data["well_name"], sort=True)
        return uniques



    

    