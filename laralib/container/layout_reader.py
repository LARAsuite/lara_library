"""_____________________________________________________________________

:PROJECT: LARA

*layout_reader library*

:details: layout reader.
          -handling barcode intervals of non numeric 
            TA1001-TA1004 -> TA1001 TA1002 TA1003 TA1004
            OR01 - OR34   -> OR01, OR02, ..., OR34
         - 

:file:    admin.py
:authors: mark doerr

:date: (creation)          20190117
:date: (last modification) 20190119

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.2"

import csv
import re
from io import StringIO
import logging
import laralib.container.barcode_handler as bh

import pandas as pd

class LayoutReader():
    """ Class doc """
    def __init__ (self, layout_filename=""):
        """ Class initialiser """
        
        self.layout_description = ""
        
        with open(layout_filename, "r") as layout_file:
            self.plate_layout = layout_file.read()
            self.parseBarcode()
            self.parseDescription()
            self.parsePlateGeometry()
            self.parseUnits()
            self.parseLayoutDescriptions()
            self.parseVolumes()
            self.parseAdditions()
            
    def parseBarcode(self):
        """trying to find a "Barcode: " string and extract the barcodes
           :param -: - """
        try:
            barcode_seq = re.search(r'(\s*#*\s*[B,b]arcodes\s*:\s*)(?P<barcodes>.*)', 
                                    self.plate_layout).group('barcodes')
        except AttributeError as err:
            logging.error("wrong barcode format: {}".format(err) )
            
        logging.debug("barcodes:{}".format(barcode_seq) )
        
        #~ self.barcodes = bh.expandBCsequence(barcode)
        #~ bc = re.split('\s*,\s*', m.group('barcodes'))
        
    def parseDescription(self):
        """ 
        :param [param_name]: [description]"""
        try:
            self.layout_description = re.search(r'(\s*#*\s*[D,d]escription\s*:\s*)(?P<description>[\w\s]+)(.*)', 
                                                self.plate_layout).group('description')
                                                
            logging.debug("self.layout_description b {}".format(self.layout_description) )
            
        except AttributeError as err:
            logging.error("wrong description format: {}".format(err) )
        
    def parsePlateGeometry(self):
        """
        # rows=8 ; columns=12, ,,,, 
        :param [param_name]: [description]"""
        
        try:
            match = re.search(r'(\s*#*\s*rows\s*=\s*)(?P<rows>\d+)(\s*;\s*columns\s*=\s*)(?P<columns>\d+)(.*)', 
                                            self.plate_layout) # .group('description')
            self.nrows = int(match.group('rows'))
            self.ncols = int(match.group('columns'))

        except AttributeError as err:
            logging.error("wrong geometry format: {}".format(err) )
        
    def parseUnits(self):
        """
          # concentration_unit: uM ; volume_unit : ul; possible units: uM mM  M uL mL L,
         :param [param_name]: [description]"""
        
        try:
            match = re.search(r'(\s*#*\s*concentration_unit\s*:\s*)(?P<conc_unit>M|uM|mM)\s*;\s*volume_unit\s*:\s*(?P<vol_unit>l|L|mL|ml|ul|uL)(.*)', 
                                            self.plate_layout)
            #~ print(match.groups())
            
            self.conc_unit = match.group('conc_unit')
            self.vol_unit = match.group('vol_unit')
            
        except AttributeError as err:
            logging.error("wrong unit format: {}".format(err) )
    
    def parseLayoutDescriptions(self):
        """ :param [param_name]: [description]"""        

        # reading nrows, starting from "HEAD_DESCR" into pandas data frame
        f = StringIO(self.plate_layout[self.plate_layout.index('HEAD_DESCR'):])
        self.descr_df = pd.read_csv(f, nrows=self.nrows, index_col=0, comment="#", engine="c")
        
        print(self.descr_df)
        
        #~ transf_df = pd.melt(self.descr_df, value_vars=['A','B','C'])
        
        self.layout_df = self.descr_df.stack().reset_index()
        
        temp_df = self.layout_df[0].str.split(":",n=1, expand= True)
        
        self.layout_df['type'] = temp_df[0]
        self.layout_df['description'] = temp_df[1]
        self.layout_df.drop(columns =[0], inplace = True)
        
        self.layout_df.rename(columns = {'HEAD_DESCR':'row', 'level_1':'col'}, inplace = True)
        self.layout_df['well'] = self.layout_df['row'] + self.layout_df['col'].str.zfill(2)
        
        print(self.layout_df)
        
        #~ df_res = pd.concat([df_st_ri, spl_df], axis=1)

    def parseVolumes(self):
        """
          # Volumes : 211,,,,,,,,,,
         :param [param_name]: [description]"""
        
        try:
            self.volumes = int( re.search(r'(\s*#*\s*[V,v]olumes\s*:\s*)(?P<volumes>\d+)(.*)', 
                                             self.plate_layout).group('volumes'))
             
        except AttributeError as err:
            logging.error("wrong volume format: {}".format(err) )
            
        self.layout_df['volumes'] = self.volumes
            
        #~ print(self.layout_df)
        
    def parseAdditions(self):
        """ :param [param_name]: [description]"""
        logging.warning("Additions impl. missing {}".format(1) )
