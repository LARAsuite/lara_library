"""_____________________________________________________________________

:PROJECT: LARA

*barcode_handler library*

:details: lara_containers barcode handling tools.
          -handling barcode intervals of non numeric 
            TA1001-TA1004 -> TA1001 TA1002 TA1003 TA1004
            OR01 - OR34   -> OR01, OR02, ..., OR34
         - 

:file:    admin.py
:authors: mark doerr

:date: (creation)          20170211
:date: (last modification) 20180625

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.3"

import logging
import re

def expandBCsequence(barcode_sequence=""):
    """ Expands a barcode interval string into a list of barcode strings.
    Possible intputs are:
    1. purly numeric, single string, e.g., "1789" will result in ["1789"]
    2. purly numeric, string interval, e.g., "1789-1792" will result in ["1789","1790", "1791","1792"]
    3. (furture, not yet implemented) mixed alphanumeric,  e.g., "SE1789-SE1792" will result in ["SE1789","SE1790", "SE1791","SE1792"]
    todo: output should be a string ! (needs some further changes 
    """
    barcode_list = []
    
    if "," in barcode_sequence:
        barcode_seq_split = re.split('\s*,\s*', barcode_sequence)
        
        barcode_seq_split = barcode_sequence.split(",")
        for bc in barcode_seq_split:
            if "-" in bc:
                barcode_interval = bc.split("-")
                for bci in range(int(barcode_interval[0]),int(barcode_interval[1])+1) :
                    barcode_list.append(bci) 
            else:
                barcode_list.append(bc)
    elif "-" in barcode_sequence:
        barcode_interval = barcode_sequence.split("-")
        barcode_list =  [ bci for bci in range(int(barcode_interval[0]),int(barcode_interval[1])+1) ]
    else :
        barcode_list.append(barcode_sequence)
        #~ check ! barcode_sequence.append(barcode_interval) # appending twice to gain an interval
        #barcode_list =  [ bci for bci in range(int(barcode_interval[0]),int(barcode_interval[1])+1) ]
    
    print
        
    return barcode_list
        
def generateBCList(barcode_interval):
    """Generates just a list of strings, containing only start and endpoint of numeric barcodes"""
    barcode_sequence = []
    if "-" in barcode_interval:
        barcode_sequence = barcode_interval.split("-")
    else :
        barcode_sequence.append(barcode_interval)
        barcode_sequence.append(barcode_interval) # appending twice to gain an interval
        
    return barcode_sequence

def resolveNameIntervals(exp_name_interval):
        """ this method ensures the order in name intervals,
            it needs to be expanded for new barcode scheme
        """
        if not "-" in exp_name_interval:
            logging.debug("no intervals defined ")
            return([exp_name_interval])
        exp_name_seq = re.match(r"(.*_)(\d*)([-])(\d*)",exp_name_interval)
        try :
            # numbers of range are in position 2 and 4 of the pattern
            return([ exp_name_seq.group(1) + str(i)  for i in  range(int(exp_name_seq.group(2)),int(exp_name_seq.group(4))+1)])
        except IndexError:
            logging.debug("no range")
            return([exp_name_interval]) 

def map96_384( coords_96: tuple, quadrant ):
    row_96 = coords_96[0] 
    col_96 = coords_96[1]
    row_index_offset = 0 if quadrant < 2 else 1
    return (row_96 * 2 + row_index_offset , col_96  * 2  + quadrant % 2 )

def map384_96( coords_384: tuple):
    # needs to be fixed
    row = coords_384[0] / 2 if coords_384[0] % 2 == 0 else coords_384[0] / 2 + 1 
    col = coords_384[1] / 2 if coords_384[0] % 2 == 0 else coords_384[0] / 2 + 1
    quadrant = 0 if coords_384[0] % 2 == 0 else coords_384[0] / 2 + 1
    return ( row, col, quadrant )

def coord2wellname(coords: tuple, padding: str = ""):
    row = coords[0] 
    col = coords[1] + 1
    return f"{chr(ord('A') + row)}{col:{padding}}"

  