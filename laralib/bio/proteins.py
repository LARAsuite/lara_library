"""
________________________________________________________________________

:PROJECT: LARA

*proteins.py - module for handling amino acid sequences*

:details: 
         - 
:authors:  Stefan Born (stefan.born@tu-berlin.de)
           mark doerr  (mark@uni-greifswald.de)

:date: (creation)          20190707

.. note:: -
.. todo:: - #    

________________________________________________________________________
"""
__version__ = "0.0.1"

import logging
import re

from Bio.PDB.Polypeptide import one_to_three


def convMutLists2mutStr1LC(orig_aa,locs,subst_aa, space='_'):
    """converts a three lists of original amino acids, loci and substituted amino acids 
       into a list of O_loc_S strings , e.g. ['G_143_A', ,,,], unsing one-letter amino acid code (1LC)
       :param [param_name]: [description]"""
       
    subst_names1L = [ o+ space +l+ space +s for (o,l,s) in zip(orig_aa,locs,subst_aa)]
       
    return subst_names1L

def convMutLists2mutStr3LC(orig_aa,locs,subst_aa, space='_'):
    """converts a 
       into a list of ORG_loc_SUB strings, e.g. ['GLY_143_ALA', ...] , unsing three-letter amino acid code (3LC)
       :param [param_name]: [description]"""
       
    subst_names3L = [ one_to_three(o) +  space  + l + space  + one_to_three(s) for (o,l,s) in zip(orig_aa,locs,subst_aa)]
       
    return subst_names3L

def mut_str_to_mut_tuple(mut_str: str):
   """convert mutation string, like L232K to tuple (232, 'L', 'K')

   :param mut_str: [description]
   :type mut_str: str
   :return: [description]
   :rtype: [type]
   .. todo: check source aa, throw error if source is not correct
   """   
   m = re.match(r'([A-Z])(\d*)([A-Z])', mut_str)
   if m is not None:
      return (int(m.group(2)), m.group(1), m.group(3) )

def mutate_seq(seq, mut_tuple):
   """exchange a certain amino acid new one based on muation string
      mind to make sequence mutable with .tomutable()

   :param seq: [description]
   :type seq: [type]
   :param mut_str: [description]
   :type mut_str: [type]
   """   
   seq_pos = int(mut_tuple[0]) - 1
   seq[seq_pos] = mut_tuple[2]

def mutate_seq_by_mut_str(seq, mut_str):
   """convenient function exchange a certain amino acid new one based on muation string
      mind to make sequence mutable with .tomutable()

   :param seq: [description]
   :type seq: [type]
   :param mut_str: [description]
   :type mut_str: [type]
   """   
   mut_tuple =  mut_str_to_mut_tuple(mut_str)
   seq_pos = int(mut_tuple[0]) - 1
   seq[seq_pos] = mut_tuple[2]