"""_____________________________________________________________________

:PROJECT: LARA

*lara_library setup *

:details: lara_library setup file for installation.
         - For installation, run:
           run pip3 install .
           or  python3 setup.py install

:file:    setup.py
:authors: mark doerr (mark.doerr@uni-greifswald.de)

:date: (creation)          
:date: (last modification) 20191016

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.1.9"

import os
import sys

from setuptools import setup, find_packages
#~ from distutils.sysconfig import get_python_lib

package_name = 'laralib'

def read(filename):
    with open(os.path.join(os.path.dirname(__file__), filename), 'r') as file:
        return file.read().strip()

install_requires = [] 
data_files = []
package_data = {}

setup(name=package_name,
    version=__version__,
    description='LARA python library of the Lab Automation Suite LARA - (lara.uni-greifswald.de/larasuite)',
    long_description=read('README.md'),
    long_description_content_type='text/x-rst',
    author='mark doerr',
    author_email='mark.doerr@uni-greifswald.de',
    keywords='lab automation, experiments, database, evaluation, visualisation, SiLA2, robots',
    url='https://gitlab.com/LARAsuite/lara_python_library',
    license='GPL',
    packages=find_packages(),
    install_requires = install_requires,
    test_suite='',
    classifiers=[  'Development Status :: 3 - Alpha',
                   'Environment :: Console',
                   'Framework :: Django :: 2.1',
                   'Intended Audience :: Developers',
                   'Intended Audience :: Science/Research',
                   'Intended Audience :: Education',
                   'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   'Programming Language :: Python :: 3.6',
                   'Programming Language :: Python :: 3.7',
                   'Topic :: Scientific/Engineering',
                   'Topic :: Scientific/Engineering :: Information Analysis',
                   'Topic :: Scientific/Engineering :: Visualization',
                   'Topic :: Scientific/Engineering :: Bio-Informatics',
                   'Topic :: Scientific/Engineering :: Chemistry'
                ],
    include_package_data=True,
    package_data = package_data,
    data_files=data_files,
)

