"""_____________________________________________________________________

:PROJECT: LARAsuite

*Thermo SkanIt6 data importer*

:details: Thermo SkanIt6 data importer

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20200628

.. note:: -
.. todo:: - 

________________________________________________________________________
"""

__version__ = "0.0.1"

import os
import unittest
import logging

from laralib.data.data_reader import DataReader

class ThermoSkanit6TestCase(unittest.TestCase):
    def setUp(self):
        print(os.getcwd())
        self.data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 
                                      'test_data',  'absorbance', 'thermo_skanit6' ) 
        self.abs_filename_pattern = "*SPabs96_600_660*.xml"
        self.thermo_skanit6_data_reader = DataReader(method='absorbance', 
                                    data_format="thermo_skanit6.uv_vis_kinetics",
                                    data_path=self.data_path, 
                                    filename_pattern=self.abs_filename_pattern)

    def test_read_dataframe(self):
        #logging.debug(f"read xml {self.thermo_skanit6_data_reader.data_file_list}")
        test_filename = "TA0041_20200622_190639_varioskanLux_SPabs96_600_660_E_P_incubation_cycle_growth_fluoresc.xml"
        self.assertEqual(self.thermo_skanit6_data_reader.data_file_list[0], 
                                     os.path.join(self.data_path, test_filename) )

        skit6_df = self.thermo_skanit6_data_reader.dataframe

        self.assertEqual(skit6_df.loc[0, "value_660_0"], 0.0512 )
        self.assertEqual(skit6_df.loc[0, "delta_time_s"], 0.0 )

        print(self.thermo_skanit6_data_reader.metadata.list_core())
        

if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()