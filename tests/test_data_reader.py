"""_____________________________________________________________________

:PROJECT: LARAsuite

*DataReader Testsuite*

:details: DataReader Testsuite

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20201022

.. note:: -
.. todo:: - 

________________________________________________________________________
"""

__version__ = "0.0.1"

import os
import unittest
import logging

from laralib.data.data_reader import DataReader

class DataReaderCase(unittest.TestCase):
    def setUp(self):
        logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
        print(os.getcwd())
        self.data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                        'test_data', 'spectrophotometer', 'jasco' ) 
        #self.abs_filename_pattern = "*SPabs96_600_660*.xml"
        #self.varioskanLux_data_reader = DataReaderVarioskanLux(data_path=self.data_path, filename_pattern=self.abs_filename_pattern)
    
    def test_register_all_data_formats(self):
        dr = DataReader(method='absorbance', data_format="bmg_omega.uv_vis_singlepoint",
                        data_path=self.data_path, filename_pattern="*.txt")
        print(dr.list_methods())
        print(dr.list_data_format_classes())
        print(dr.list_data_format_subclasses())
        print(dr.list_data_formats())

    def test_read_data_df(self):
        # "bmg_omega.uv_vis_singlepoint"
        dr = DataReader(method='absorbance', data_format="jasco.uv_vis_kinetics",
                        data_path=self.data_path, filename_pattern="*.txt")
        print(dr.dataframe)
        print(dr.metadata.list_core())

if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()