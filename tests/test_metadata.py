"""_____________________________________________________________________

:PROJECT: LARAsuite

*MetaData Testsuite*

:details: MetaData Testsuite

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20201022

.. note:: -
.. todo:: - 

________________________________________________________________________
"""

__version__ = "0.0.1"

import os
import unittest
import logging

from laralib.data.metadata import MetaData, MetaDataIncompleteError

class MetaDataTestCase(unittest.TestCase):
    def setUp(self):
        print(os.getcwd())
        self.data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                        'test_data', 'spectrophotometer', 'jasco' ) 
        #self.abs_filename_pattern = "*SPabs96_600_660*.xml"
        #self.varioskanLux_data_reader = MetaDataVarioskanLux(data_path=self.data_path, filename_pattern=self.abs_filename_pattern)
    
    def test_core(self):
        #logging.debug(f"read xml {self.varioskanLux_data_reader.data_file_list}")
        md = MetaData()
        md.core.user = 'Ben'
        self.assertEqual(md.core.user, 'Ben')
        # testing adding a wrong attribute
        with self.assertRaises(AttributeError):
             md.core.not_in_core = "not in core"
        # testing completeness
        with self.assertRaises(AttributeError):
            md.core.timestamp
        with self.assertRaises(MetaDataIncompleteError):
            for item in md.core.items():
                pass
        # adding default values to core and test them
        for attr in md.core:
            md.core[attr] = "def value"

        for item in md.core.items():
            print(item)
        self.assertEqual(md.core.meas_software_version, "def value")

if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()